﻿using System;
using MySql.Data.MySqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace profit
{
    [TestClass]
    public class MitarbeiterinnenTest
    {
    }

    [TestClass]
    public class ProjektTest
    {
        [TestMethod]
        public void InitialisierungProjektBeteiligteExists()
        {
            // Act
            Projekt p = new Projekt();

            // Assert
            Assert.IsNotNull(p.Beteiligungen);
        }

        [TestMethod]
        public void InitialisierungProjektBeteiligteExists2()
        {
            // Act
            Projekt p = new Projekt(0, "InitialisierungProjektBeteiligteExists2", new DateTime(), new DateTime(), 0.0);

            // Assert
            Assert.IsNotNull(p.Beteiligungen);
        }

        [TestMethod]
        public void DauerInWochen2Wochen()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2022, 6, 8), new DateTime(2022, 6, 22), 0.0);

            // Act
            double result = p.DauerInWochen();

            // Assert
            Assert.AreEqual(2.0, result);
        }

        [TestMethod]
        public void DauerInWochenEinTag()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 7), new DateTime(2021, 6, 8), 0.0);

            // Act
            double result = p.DauerInWochen();

            // Assert
            Assert.AreEqual(1.0 / 7, result);
        }

        [TestMethod]
        public void DauerInWochenGleicherTag()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 8), 0.0);

            // Act
            double result = p.DauerInWochen();

            // Assert
            Assert.AreEqual(result, 0.0);
        }

        [TestMethod]
        public void DauerInWochenNegativerZeitraum()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 1), 0.0);

            // Act
            double result = p.DauerInWochen();

            // Assert
            Assert.AreEqual(result, -1.0);
        }

        [TestMethod]
        public void PersonenStunden2Ma40Stunden2Wochen()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2022, 6, 8), new DateTime(2022, 6, 22), 0.0);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma1, 40));
            Mitarbeiterin ma2 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma2, 40));

            // Act
            double result = p.PersonenStunden();

            // Assert
            Assert.AreEqual(160, result);
        }

        [TestMethod]
        public void PersonenStunden3MaUnterschiedlicheAnteile4Wochen()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 7, 6), 0.0);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma1, 40));
            Mitarbeiterin ma2 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma2, 20));
            Mitarbeiterin ma3 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma3, 20));

            // Act
            double result = p.PersonenStunden();

            // Assert
            Assert.AreEqual(result, 320);
        }

        [TestMethod]
        public void Rentabilitaet1000Euro2Wochen40Stunden()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1000);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma1, 40));

            // Act
            double result = p.Rentabilitaet();

            // Assert
            Assert.AreEqual(result, 1000.0 / 80.0);
        }

        [TestMethod]
        public void Rentabilitaet0Euro2Wochen40Stunden()
        {
            // Arrange
            Projekt p = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 0);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p.Beteiligungen.Add(new Beteiligung(ma1, 100));

            // Act
            double result = p.Rentabilitaet();

            // Assert
            Assert.AreEqual(result, 0);
        }
    }

    [TestClass]
    public class BeteiligungTest
    {
    }

    [TestClass]
    public class ProjektverwaltungTest
    {
        [TestMethod]
        public void InitialisierungVerwaltungProjektlisteExists()
        {
            // Act
            Projektverwaltung pv = new Projektverwaltung();

            // Assert
            Assert.IsNotNull(pv.ProjektListe);
        }

        [TestMethod]
        public void RentabelstesGleicherWertUnterschiedlicheDauer()
        {
            // Arrange
            Projektverwaltung pv = new Projektverwaltung();

            Projekt p1 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1000);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p1.Beteiligungen.Add(new Beteiligung(ma1, 100));
            pv.ProjektListe.Add(p1);

            Projekt p2 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 21), 1000);
            Mitarbeiterin ma2 = new Mitarbeiterin();
            p2.Beteiligungen.Add(new Beteiligung(ma2, 100));
            pv.ProjektListe.Add(p2);

            // Act
            Projekt result = pv.BestimmeRentabelstesProjekt();

            // Assert
            Assert.AreEqual(result, p2);
        }

        [TestMethod]
        public void RentabelstesUnterschiedlicherWertGleicheDauer()
        {
            // Arrange
            Projektverwaltung pv = new Projektverwaltung();

            Projekt p1 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1000);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p1.Beteiligungen.Add(new Beteiligung(ma1, 100));
            pv.ProjektListe.Add(p1);

            Projekt p2 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1001);
            Mitarbeiterin ma2 = new Mitarbeiterin();
            p2.Beteiligungen.Add(new Beteiligung(ma2, 100));
            pv.ProjektListe.Add(p2);

            // Act
            Projekt result = pv.BestimmeRentabelstesProjekt();

            // Assert
            Assert.AreEqual(result, p2);
        }

        [TestMethod]
        public void RentabelstesGleicherWertGleicheDauer()
        {
            // Arrange
            Projektverwaltung pv = new Projektverwaltung();

            Projekt p1 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1000);
            Mitarbeiterin ma1 = new Mitarbeiterin();
            p1.Beteiligungen.Add(new Beteiligung(ma1, 100));
            pv.ProjektListe.Add(p1);

            Projekt p2 = new Projekt(0, "", new DateTime(2021, 6, 8), new DateTime(2021, 6, 22), 1000);
            Mitarbeiterin ma2 = new Mitarbeiterin();
            p2.Beteiligungen.Add(new Beteiligung(ma2, 100));
            pv.ProjektListe.Add(p2);

            // Act
            Projekt result = pv.BestimmeRentabelstesProjekt();

            // Assert
            Assert.AreEqual(result, p1);
        }

        [TestMethod]
        public void RentabelstesTestDB()
        {
            // Arrange

            Projektverwaltung pv = new Projektverwaltung();

            string query1 = @"SELECT * FROM projekt";
            string query2 = @"SELECT * FROM projektmitarbeiter WHERE ProjNr = @ProjNr";
            string query3 = @"SELECT * FROM mitarbeiter WHERE MNr = @MNr";

            string connectionString = @"SERVER=localhost;DATABASE=projektverwaltung;UID=root";

            using (MySqlConnection connection1 = new MySqlConnection(connectionString))
            {
                MySqlCommand cmd1 = new MySqlCommand(query1, connection1);
                connection1.Open();

                using (MySqlDataReader PDR = cmd1.ExecuteReader())
                {
                    while (PDR.Read())
                    {
                        Projekt p = new Projekt((int)PDR["ProjNr"], (string)PDR["Bezeichnung"], (DateTime)PDR["Beginn"], (DateTime)PDR["Ende"], Convert.ToDouble(PDR["Auftragswert"]));

                        using (MySqlConnection connection2 = new MySqlConnection(connectionString))
                        {

                            MySqlCommand cmd2 = new MySqlCommand(query2, connection2);
                            cmd2.Parameters.AddWithValue("ProjNr", (int)PDR["ProjNr"]);
                            connection2.Open();

                            using (MySqlDataReader BDR = cmd2.ExecuteReader())
                            {
                                while (BDR.Read())
                                {

                                    Mitarbeiterin m = null;

                                    using (MySqlConnection connection3 = new MySqlConnection(connectionString))
                                    {

                                        MySqlCommand cmd3 = new MySqlCommand(query3, connection3);
                                        cmd3.Parameters.AddWithValue("MNr", (int)BDR["MNr"]);
                                        connection3.Open();

                                        using (MySqlDataReader MDR = cmd3.ExecuteReader())
                                        {
                                            while (MDR.Read())
                                            {
                                                m = new Mitarbeiterin((int)MDR["MNr"], (string)MDR["Name"], (string)MDR["Vorname"], (DateTime)MDR["eingestellt"]);
                                            }
                                        }
                                        p.Beteiligungen.Add(new Beteiligung(m, (int)BDR["Zeitanteil"]));
                                    }
                                }
                                pv.ProjektListe.Add(p);
                            }
                        }
                    }
                }
            }

            // Act
            Projekt result = pv.BestimmeRentabelstesProjekt();

            // Assert
            Assert.AreEqual(result.PNR, 79);
        }
    }
}