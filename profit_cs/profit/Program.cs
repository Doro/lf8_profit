﻿using System;
using System.Collections.Generic;

namespace profit
{
    public class Projekt
    {
        // Attribute
        private int _pnr;
        private String _bezeichnung;
        private double _auftragswert;
        private bool _bezahlt;
        private DateTime _beginnDatum;
        private DateTime _endDatum;
        private bool _storno;

        private Mitarbeiterin _leiterin;
        private List<Beteiligung> _beteiligungen;

        // Get/Set
        public int PNR
        {
            get { return _pnr; }
            set { _pnr = value; }
        }

        public bool Stono        
        {
            get { return _storno; }
            set { _storno = value; }
        }

        public List<Beteiligung> Beteiligungen
        {
            get { return _beteiligungen; }
            set { _beteiligungen = value; }
        }

        // Konstruktoren
        public Projekt()
        {
            _beteiligungen = new List<Beteiligung>();
        }

        public Projekt(int pnr, String bezeichnung, DateTime beginn, DateTime end, double wert)
        {
            _pnr = pnr;
            _bezeichnung = bezeichnung;
            _beginnDatum = beginn;
            _endDatum = end;
            _auftragswert = wert;
            _beteiligungen = new List<Beteiligung>();
        }

        // Methoden
        /// <remarks>Berechnen sich die Wochen mit 7 Tagen oder 5 Arbeitstagen?</remarks>
        /// <remarks>Bei den Zeiträumen ist der letzte Tag ausschließlich.</remarks>
        public double DauerInWochen()
        {
            TimeSpan dauer = _endDatum - _beginnDatum;
            return dauer.TotalDays / 7.0;
        }

        public double PersonenStunden()
        {
            double summeStunden = 0.0;

            foreach (Beteiligung b in _beteiligungen)
            {
                summeStunden = summeStunden + b.Anteil;
            }
            return summeStunden * DauerInWochen();
        }

        public double Rentabilitaet()
        {
            double rentabilitaet = _auftragswert / PersonenStunden(); 

            return rentabilitaet;
        }
    }

    public class Mitarbeiterin
    {
        // Attribute
        private int _mnr;
        private string _nachname;
        private string _vorname;
        private DateTime _einstellung;

        // Konstruktoren
        public Mitarbeiterin()
        {

        }

        public Mitarbeiterin(int mnr, string nname, string vname, DateTime einstellung)
        {
            _mnr = mnr;
            _nachname = nname;
            _vorname = vname;
            _einstellung = einstellung;
        }
    }

    public class Beteiligung
    {
        private Mitarbeiterin _mitarbeiterin;
        private int _anteil;

        // Get/Set
        public int Anteil
        {
            get { return _anteil; }
            set { _anteil = value;  }
        }

        // Konstruktoren
        public Beteiligung(Mitarbeiterin mitarbeiterin, int anteil)
        {
            _mitarbeiterin = mitarbeiterin;
            _anteil = anteil;
        }

    }

    public class Projektverwaltung
    {
        // Attribute
        private List<Projekt> _projektListe;

        // Get/Set
        public List<Projekt> ProjektListe
        {
            get { return _projektListe; }
            set { _projektListe = value; }
        }

        // Konstruktoren
        public Projektverwaltung()
        {
            _projektListe = new List<Projekt>();
        }

        public Projekt BestimmeRentabelstesProjekt()
        {
            double rentabilitaet = 0;
            Projekt gewinner = null;

            foreach (Projekt p in _projektListe) { 
                if (!p.Stono && p.Rentabilitaet() > rentabilitaet)
                {
                    rentabilitaet = p.Rentabilitaet();
                    gewinner = p;
                } 
            }

            return gewinner;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        
        }
    }
}